import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroListComponent } from './Hero/hero-list/hero-list.component';
import { HeroEditModalComponent } from './Hero/hero-edit-modal/hero-edit-modal.component';

const routes: Routes = [
  { path: 'hero', component: HeroListComponent },
  { path: 'hero/edit', component: HeroEditModalComponent },
  { path: '', redirectTo: '/hero', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
