export class Species {
    id: Number;
    name: string;
    desc: string;
}