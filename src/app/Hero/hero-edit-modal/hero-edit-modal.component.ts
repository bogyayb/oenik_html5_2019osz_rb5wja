import { Component, OnInit, Inject, Optional} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HeroService } from 'src/app/Services/hero.service';
import { Hero } from 'src/app/Models/Hero';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-hero-edit-modal',
  templateUrl: './hero-edit-modal.component.html',
  styleUrls: ['./hero-edit-modal.component.css']
})

export class HeroEditModalComponent implements OnInit  {
  
  //fields
  id: string;

  //FORM fields
  heroForm = new FormGroup({
    name: new FormControl(''),
    desc: new FormControl('')
  });

  //constructor
  constructor(private dataService: HeroService,
              private _snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<HeroEditModalComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
      this.id = data.id;
  }

  //inicialization
  ngOnInit() {
    this.getHero();
  }

  //methods
  onSubmit() {
    var hero: Hero = {
      id: this.id,
      name: this.heroForm.value.name,
      description: this.heroForm.value.desc,
    };

    this.dataService.updateHero(this.id, hero).subscribe((data: Hero)=>{
      this.openSnackBar("Sikeres szerkesztés!", "✅");
    })
  
  }  

  //API requests
  getHero() {
    this.dataService.getHero(this.id).subscribe((data: Hero)=>{      
      //update FORM value
      this.heroForm.patchValue({
        name: data.name,
        desc: data.description
      });
    })
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
