import { Component, OnInit, Inject, Optional} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HeroService } from 'src/app/Services/hero.service';
import { Hero } from 'src/app/Models/Hero';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-hero-add-modal',
  templateUrl: './hero-add-modal.component.html',
  styleUrls: ['./hero-add-modal.component.css']
})

export class HeroAddModalComponent implements OnInit  {
  
  //FORM fields
  heroForm = new FormGroup({
    name: new FormControl(''),
    desc: new FormControl('')
  });

  //constructor
  constructor(private dataService: HeroService,
              private _snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<HeroAddModalComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  //inicialization
  ngOnInit() {
    
  }

  //methods
  onSubmit() {
    var hero: Hero = {
      id: null,
      name: this.heroForm.value.name,
      description: this.heroForm.value.desc,
    };

    this.dataService.addHero(hero).subscribe((data: Hero)=>{
      this.openSnackBar("Sikeres Létrehozás!", "✅");
    })
  
  }  

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
