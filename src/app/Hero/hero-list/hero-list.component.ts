import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { HeroService } from 'src/app/Services/hero.service';
import { MatDialog } from '@angular/material/dialog';
import { HeroEditModalComponent } from '../hero-edit-modal/hero-edit-modal.component';
import { Hero } from 'src/app/Models/Hero';
import { HeroAddModalComponent } from '../hero-add-modal/hero-add-modal.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})

export class HeroListComponent implements OnInit {
  
  //fields
  displayedColumns: string[] = ['id', 'name', 'description', 'edit'];
  dataSource = new MatTableDataSource();
  newHero: Hero;

  //childs
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  //constructor
  constructor(private dataService: HeroService, public dialog: MatDialog, private _snackBar: MatSnackBar) { }

  //inicialization
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getHeroes();
  }

  //methods
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  //dialogs
  editHero(id: number) {

    const dialogRef = this.dialog.open(HeroEditModalComponent, {
          data: {id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getHeroes();
    });
  }

  addHero() {
    
    const dialogRef = this.dialog.open(HeroAddModalComponent, {
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.getHeroes();
    });
  }

  //API requests
  getHeroes() {
    this.dataService.getHeroes().subscribe((data: Hero[])=>{
      this.dataSource.data = data;

    })  
  }

  deleteHero(id: number) {
    this.dataService.deleteHero(id).subscribe((data: Hero)=>{
      this.getHeroes();
      this.openSnackBar("Sikeres törlés!", "✅");
    })  
  }

  //snack bar
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}