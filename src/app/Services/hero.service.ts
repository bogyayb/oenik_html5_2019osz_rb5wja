import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Hero } from '../Models/Hero';

const localUrl = 'http://81.2.241.234:8080';
const options = {headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')};

@Injectable({
  providedIn: 'root'
})

export class HeroService {

  //constructor
  constructor(private httpClient: HttpClient) { }

  //API-s
  public getHeroes(){
    return this.httpClient.get(localUrl + "/hero"+"?count=1000000");
  }

  public addHero(hero: Hero){   
    return this.httpClient.post(localUrl + "/hero", this.convert(hero), options);
  }

  public getHero(id: string){
    return this.httpClient.get(localUrl + "/hero/" + id);
  }

  public updateHero(id: string, hero: Hero){
    return this.httpClient.put(localUrl + "/hero/" + id, this.convert(hero), options);
  }

  public deleteHero(id: number){
    return this.httpClient.delete(localUrl + "/hero/" + id);
  }

  //helper methods
  public convert(hero: Hero){    
    var body = new URLSearchParams();
      body.set('name', hero.name);
      body.set('desc', hero.description);

    return body.toString();
  }

}
